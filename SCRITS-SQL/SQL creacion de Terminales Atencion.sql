

use sca

-- en este script vamos a crear y administrar todo lo referente a los escritorios de atencion

-- tabla Escritorios de Atencion
CREATE TABLE EscritoriosDeTrabajo(
 ID_ESCRITORIO int IDENTITY(1,1) PRIMARY KEY,
 NombreTerminal nvarchar(50) not null,
 EstadoTerminal nvarchar(50) not null,
);

-- detalle de escritorios
CREATE TABLE DETALLE_ESCRITORIOS(
 ID_Detalle_ESCRITORIO int IDENTITY(1,1) PRIMARY KEY,
 ID_ESCRITORIO int not null,
 ID_DETALLE_TICKET int not null,
 Usuario nvarchar(50) not null,

 CONSTRAINT fk_relacion_Escritorios
FOREIGN KEY (ID_ESCRITORIO) REFERENCES EscritoriosDeTrabajo(ID_ESCRITORIO),
CONSTRAINT fk_relacion_detallesTK
FOREIGN KEY (ID_DETALLE_TICKET) REFERENCES DETALLE_Tickets(ID_Detalle_Ticket)
);

INSERT INTO EscritoriosDeTrabajo(NombreTerminal,EstadoTerminal)
	values ('Computadora N1','Activa'),('Computadora N2','Activa')

-- creando procedimiento almacenado ATENDIENDO UNA TICKET
GO
CREATE PROCEDURE LLamandoTk_Atendiendo
@descripcion_codTK nvarchar(50),
@fecha_actual datetime,
@usuario nvarchar(50),
@NombreTerminal nvarchar(50)
as

update DETALLE_Tickets SET EstadoTK = 'Atendiendo' where DETALLE_Tickets.DescripCodTK =@descripcion_codTK
		and DETALLE_Tickets.VENCIMIENTO_TK ='ACTIVA' AND DETALLE_Tickets.FechaExpiracion >= @fecha_actual

select top(1) DETALLE_Tickets.DescripCodTK from DETALLE_Tickets where
		DETALLE_Tickets.VENCIMIENTO_TK ='ACTIVA' AND DETALLE_Tickets.FechaExpiracion >= @fecha_actual
		AND DETALLE_Tickets.EstadoTK != 'Atendido' 
		ORDER BY DETALLE_Tickets.FechaExpiracion ASC

		BEGIN
			declare @ID_DETALLETK int;
			declare @ID_ESCRITORIO INT

			select @ID_DETALLETK = DETALLE_Tickets.ID_Detalle_Ticket FROM
	DETALLE_Tickets where DETALLE_Tickets.VENCIMIENTO_TK ='ACTIVA' AND DETALLE_Tickets.FechaExpiracion >= @fecha_actual
		AND DETALLE_Tickets.DescripCodTK = @descripcion_codTK

		SELECT @ID_ESCRITORIO = EscritoriosDeTrabajo.ID_ESCRITORIO FROM EscritoriosDeTrabajo
		WHERE EscritoriosDeTrabajo.NombreTerminal =@NombreTerminal and 
		EscritoriosDeTrabajo.EstadoTerminal = 'Activa'

		INSERT INTO DETALLE_ESCRITORIOS(ID_ESCRITORIO, ID_DETALLE_TICKET,
		Usuario) 
		values (@ID_ESCRITORIO, @ID_DETALLETK,@usuario)
		END
GO

-- PROCEDIENTO ALMACENADO PARA MARCAR TICKET COMO  ATENDIDO
GO 
CREATE PROCEDURE LLamandoTk_Atendido
@descripcion_codTK nvarchar(50)

	as
	update DETALLE_Tickets SET EstadoTK = 'Atendido', VENCIMIENTO_TK = 'VENCIDA'
	where DETALLE_Tickets.DescripCodTK = @descripcion_codTK
	
GO

-- PROCEDIENTO ALMACENADO PARA MARCAR TICKET COMO retirada
GO 
CREATE PROCEDURE LLamandoTk_RETIRO
@descripcion_codTK nvarchar(50)

	as
	update DETALLE_Tickets SET EstadoTK = 'Retiro'
	where DETALLE_Tickets.DescripCodTK = @descripcion_codTK
	
GO

-- PROCEDIMEINTO PARA VOLVER A LLAMAR
GO 
CREATE PROCEDURE LLamandoTk_VolverLlamar
@descripcion_codTK nvarchar(50)

	as
	update DETALLE_Tickets SET EstadoTK = 'En Espera'
	where DETALLE_Tickets.DescripCodTK = @descripcion_codTK
	
GO
-- procedimeinto para comprobar que la ticket existe y esta activa
GO
CREATE PROCEDURE VerificarSi_existeTK
@DescripcionCod_tk nvarchar(50),
@fecha_actual datetime 
as
	select top(1) DETALLE_Tickets.DescripCodTK from DETALLE_Tickets where
		DETALLE_Tickets.VENCIMIENTO_TK ='ACTIVA' 
		AND CONVERT(DATE, DETALLE_Tickets.FechaExpiracion) = CONVERT(DATE, @fecha_actual)
		AND DETALLE_Tickets.EstadoTK != 'Atendido' 
		ORDER BY DETALLE_Tickets.FechaExpiracion ASC
		
GO

-- verificar escritorios

GO
CREATE PROCEDURE VerificarSi_existeESCRITORIO
@nombreterminal nvarchar(50)
as
	select top(1) EscritoriosDeTrabajo.ID_ESCRITORIO FROM EscritoriosDeTrabajo
		WHERE EscritoriosDeTrabajo.NombreTerminal = @Nombreterminal and 
		EscritoriosDeTrabajo.EstadoTerminal = 'Activa'
		
GO

-- probando insertar y atender una ticket

execute LLamandoTk_Atendiendo 'C-1','15.01.2024','enmanuel357@hotmail.com', 'Computadora N1'

execute LLamandoTk_Atendido 'AL-13'
EXECUTE LLamandoTk_RETIRO 'AL-11'
EXECUTE LLamandoTk_VolverLlamar 'AL-11'

--verifiando que exista la tk
EXECUTE VerificarSi_existeTK 'AL-11','09.01.2024'
EXECUTE VerificarSi_existeESCRITORIO 'Computadora n1'

SELECT * FROM DETALLE_Tickets where DETALLE_Tickets.DescripCodTK='AL-13'
select * from DETALLE_ESCRITORIOS
SELECT * FROM MAE_Tickets
select * from CAT_SERVICIO

select top(1) DETALLE_Tickets.DescripCodTK from DETALLE_Tickets where
		DETALLE_Tickets.VENCIMIENTO_TK ='ACTIVA' 
		AND DETALLE_Tickets.FechaExpiracion > GETDATE() AND CONVERT(DATE, DETALLE_Tickets.FechaExpiracion) = CONVERT(DATE, GETDATE())
		AND DETALLE_Tickets.EstadoTK != 'Atendido' 
		ORDER BY DETALLE_Tickets.FechaExpiracion ASC

update DETALLE_Tickets SET EstadoTK = 'Atendido' where DETALLE_Tickets.DescripCodTK ='AL-6'
update DETALLE_Tickets SET EstadoTK = 'Atendido', VENCIMIENTO_TK = 'VENCIDA' 
where DETALLE_Tickets.DescripCodTK ='AL-9'

update MAE_Tickets SET COD_TK = 'C'
where MAE_Tickets.ID_Ticket =25