USE [SCA]
GO
/****** Object:  StoredProcedure [dbo].[pa_GuardarRegistroNuevoVisitante]    Script Date: 14/10/2023 23:21:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[pa_GuardarRegistroNuevoVisitante]
	@COD_COMPLEJO NVARCHAR (10),
	@COD_PUERTA nvarchar(10),
	@COD_TIPO_IDENTIFICACION nvarchar(2),
	@IDENTIFICACION nvarchar(30),
	@NOMBRE nvarchar(200),
	@SEXO nvarchar(5),
	@DIRECCION nvarchar(500)
AS
BEGIN TRY
	
	INSERT INTO [TBL_NUEVO_VISITANTE]
           ([COD_PUERTA]
           ,[COD_COMPLEJO]
           ,[COD_TIPO_IDENTIFICACION]
           ,[IDENTIFICACION]
           ,[NOMBRE]
           ,[SEXO]
           ,[DIRECCION])
     VALUES
           (@COD_PUERTA
           ,@COD_COMPLEJO
           ,@COD_TIPO_IDENTIFICACION
           ,@IDENTIFICACION
           ,@NOMBRE
           ,@SEXO
           ,@DIRECCION)

		select ID_NUEVO_VISITANTE
		from TBL_NUEVO_VISITANTE where TBL_NUEVO_VISITANTE.IDENTIFICACION=@IDENTIFICACION
		ORDER BY ID_NUEVO_VISITANTE DESC 

END TRY

BEGIN CATCH
	
		SELECT
			ERROR_NUMBER() AS ErrorNumber,
			ERROR_SEVERITY() AS ErrorSeverity,
			ERROR_STATE() AS ErrorState,
			ERROR_PROCEDURE() AS ErrorProcedure,
			ERROR_LINE() AS ErrorLine,
			ERROR_MESSAGE() AS ErrorMessage
	
END CATCH

USE SCA

EXECUTE pa_GuardarRegistroNuevoVisitante '1','1','01','4411507940013A','Maria Gomez','M',
'Bomba de agua, Matagalpa'

-- script para crear tabla de detalle de acompañantes donde la llave foreanea 
-- sera el ID_NUEVO_VISITANTE

Create table Detalle_Visitante(
ID_NUEVO_VISITANTE int not null,
Identificacion nvarchar(100) not null,
Nombre nvarchar(100) not null,
Sexo nvarchar(10) not null,
Direccion nvarchar(100) not null,
CONSTRAINT fk_detallevisitante_nuevovisitante 
FOREIGN KEY (ID_NUEVO_VISITANTE) REFERENCES [SCA].[dbo].[TBL_NUEVO_VISITANTE] (ID_NUEVO_VISITANTE)
);


--creando procedimiento almacenado para la tabla detalle visitante
go  
   CREATE PROCEDURE pa_GuardarDetalleNuevoVisitante
	@ID_NUEVO_VISITANTE int,
	@IDENTIFICACION nvarchar(30),
	@NOMBRE nvarchar(200),
	@SEXO nvarchar(5),
	@DIRECCION nvarchar(500)
   as
   INSERT INTO Detalle_Visitante
     VALUES
           (@ID_NUEVO_VISITANTE
           ,@IDENTIFICACION
           ,@NOMBRE
           ,@SEXO
           ,@DIRECCION)
go

-- EJECUTANDO EL SP ANTERIOR

EXECUTE pa_GuardarDetalleNuevoVisitante '12303','4411507940006A','Enqrique Quiñonez',
'F','Frente al mercado mayoreo'

-- consulta sql para mostrar los visitantes con sus acompañantes
SELECT TBL_NUEVO_VISITANTE.IDENTIFICACION AS CED_VISITANTE,
TBL_NUEVO_VISITANTE.NOMBRE AS DATOS_VISITANTE,
Detalle_Visitante.Identificacion as CED_ACOMPAÑANTE,
Detalle_Visitante.Nombre As ACOMPAÑANTE
FROM Detalle_Visitante INNER JOIN TBL_NUEVO_VISITANTE 
ON Detalle_Visitante.ID_NUEVO_VISITANTE=TBL_NUEVO_VISITANTE.ID_NUEVO_VISITANTE;

-- buscando un id
SELECT TBL_NUEVO_VISITANTE.IDENTIFICACION AS CED_VISITANTE,
TBL_NUEVO_VISITANTE.NOMBRE AS DATOS_VISITANTE,
Detalle_Visitante.Identificacion as CED_ACOMPAÑANTE,
Detalle_Visitante.Nombre As ACOMPAÑANTE
FROM Detalle_Visitante INNER JOIN TBL_NUEVO_VISITANTE 
ON Detalle_Visitante.ID_NUEVO_VISITANTE=TBL_NUEVO_VISITANTE.ID_NUEVO_VISITANTE
where TBL_NUEVO_VISITANTE.ID_NUEVO_VISITANTE= 12304

SELECT * FROM DBO.TBL_NUEVO_VISITANTE 
--se inserto el ULTIMO REGISTRO id 12310
