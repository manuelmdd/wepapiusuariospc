

use SCA
-- no responde correctamente este sp
EXECUTE pa_TicketGenerarNuevo '05','01','01','01','enmanuel','01','Registro Managua',1,'Guardado'


select * from TBL_MAE_REGISTRO_INGRESO where COD_MAE_REGISTRO_INGRESO = 143

-- creacion de tabla de MAESTRO Tickes

create table MAE_Tickets (
		ID_Ticket int IDENTITY(1,1) PRIMARY KEY,
		COD_TK nvarchar(10) not null,
		numeracionFinalTK int not null,
		numeracionInicial int not null,
		ultimaNumeracion int not null,
		fechaCreacion datetime not null,
		userLogin nvarchar(50) not null,
);

-- detalle tickets
CREATE TABLE DETALLE_Tickets(
 ID_Detalle_Ticket int IDENTITY(1,1) PRIMARY KEY,
 ID_Ticket int not null,
 
 DescripCodTK nvarchar(50) not null,
 FechaExpiracion datetime not null,
 EstadoTK nvarchar(50) not null,

 CONSTRAINT fk_detalleMAETICKET
FOREIGN KEY (ID_Ticket) REFERENCES MAE_Tickets(ID_Ticket)
);

-- 1 modificando el detalle de las tickes quitando la columna cod servicio

ALTER TABLE DETALLE_Tickets
DROP CONSTRAINT fk_detalleServicios

-- 2 ahora eliminamos la columna
ALTER TABLE DETALLE_Tickets
DROP COLUMN COD_SERVICIO
-- columna eliminada con exito

SELECT * FROM MAE_Tickets

SELECT * FROM DETALLE_Tickets

EXECUTE pa_listarAreasIngresar2

-- creando procedimiento almacenado
-- agregadno id de visitante como fk

ALTER TABLE DETALLE_Tickets 
	ADD ID_NUEVO_VISITANTE INT NOT NULL,
	CONSTRAINT FK_UNVISITANTEMUCHASTICKES FOREIGN KEY (ID_NUEVO_VISITANTE)
	REFERENCES TBL_NUEVO_VISITANTE(ID_NUEVO_VISITANTE)

-- modificando la tabla MAE TICKETS
ALTER TABLE MAE_Tickets
	add COD_SERVICIO nvarchar(5) not null,
	CONSTRAINT fk_detalleServicios
FOREIGN KEY (COD_SERVICIO) REFERENCES CAT_SERVICIO(COD_SERVICIO)
-- creando sp para insertar la nueva tickets

go
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[pa_nuevaTicketsRegistroPub]
	@COD_TK nvarchar(10),
	@numeracionFinalTK int,
	@numeracionInicial int,
	@ultimaNumeracion int,
	@fechaCreacion datetime,
	@userLogin nvarchar(50),
	@COD_SERVICIO nvarchar(5)
	as
INSERT INTO MAE_Tickets (COD_TK,numeracionFinalTK,numeracionInicial,ultimaNumeracion,fechaCreacion,
userLogin,COD_SERVICIO)
values (@COD_TK,@numeracionFinalTK,@numeracionInicial,@ultimaNumeracion,@fechaCreacion,
@userLogin,@COD_SERVICIO) -- SELE MODIFICO UN SELECT AL FINAL REVISAR DESDE LA BD

EXECUTE pa_nuevaTicketsRegistroPub 'VI',1000,1,1,
'16.12.2023','enmanuelarauz81@hotmail.com','027'

EXECUTE pa_listarAreasIngresar2

-- procedimiento para mostrar una ticket del maestro

go 
create procedure pa_mostrarticket_MAESTRO 
@ID_TK INT
AS
SELECT ID_Ticket, COD_TK ,numeracionFinalTK, numeracionInicial, ultimaNumeracion, fechaCreacion,
userLogin, MAE_Tickets.COD_SERVICIO, CAT_SERVICIO.DES_SERVICIO AS AREAVISITAR 
FROM MAE_Tickets INNER JOIN CAT_SERVICIO
ON MAE_Tickets.COD_SERVICIO=CAT_SERVICIO.COD_SERVICIO
WHERE MAE_Tickets.ID_Ticket = @ID_TK
GO

EXECUTE pa_mostrarticket_MAESTRO 13 -- por ID DE TICKET MAESTRO

--delete MAE_Tickets where ID_Ticket =12

go
create procedure comprobar_codServ 
@cod_servicio varchar(10)
as
SELECT 
ISNULL(CAT_SERVICIO.COD_SERVICIO,'0') AS COD_SERV
FROM CAT_SERVICIO 
WHERE CAT_SERVICIO.COD_SERVICIO=@cod_servicio
go
-- metodo para comprobar que existe el cod de servicio
EXECUTE comprobar_codServ 'enma'

SELECT * FROM DBO.TBL_NUEVO_VISITANTE 
--se inserto el ULTIMO REGISTRO id 12310

execute pa_buscarTKmaestroXareavisitar 'asesoria'
execute sp_mostrarvisitantes_acompaņantes 12310

SELECT * FROM DETALLE_Tickets
SELECT * FROM MAE_Tickets
select * from DETALLE_ESCRITORIOS
EXECUTE pa_listarAreasIngresar2

-- SP para sumar 1 al detalle de las tickets 
EXECUTE sumarenUNoUltimaNumeracion 123

-- modificando tabla detalle ticket donde se creara un nuevo campo que diga 
-- que tikets estan vencidas y cuales estan activas

ALTER TABLE DETALLE_Tickets 
	ADD VENCIMIENTO_TK VARCHAR(50)
-- Actualizando el valor  de las tickes vencidas
update DETALLE_Tickets set VENCIMIENTO_TK = 'VENCIDA' Where
DETALLE_Tickets.FechaExpiracion < '08.01.2024'

execute pa_guardarTicketDetalle 22,'AL-7','08.01.2024','En Espera',12310

update MAE_Tickets set ultimaNumeracion = 8 where ID_Ticket = 22
