USE SCA

-- insertando los nuevos servicios de la base de datos

SELECT [ID_SERVICIO]
      ,[COD_SERVICIO]
      ,[FECHA_REGISTRO]
      ,[DES_SERVICIO]
      ,[DES_CORTA_SERVICIO]
      ,[SERIE]
      ,[ACTIVO]
  FROM [SCA].[dbo].[CAT_SERVICIO] where ID_SERVICIO>=1033 and ID_SERVICIO<=1049
-- nuestros servivios inician en el cod_servicio de 027
  Insert into [SCA].[dbo].[CAT_SERVICIO]
values 
(
'028','20.10.2023 12:02:00','Ventanilla Preferencial','Ventanilla Finaciera','SV','S'
);

  Insert into [SCA].[dbo].[CAT_SERVICIO]
values 
(
'029','20.10.2023 12:02:00','Tasaci�n','Ventanilla Finaciera','SV','S'
),
(
'030','20.10.2023 12:02:00','Recepci�n','Recepci�n','SR','S'
),
(
'031','20.10.2023 12:02:00','Entrega','Recepci�n','SR','S'
),
(
'032','20.10.2023 12:02:00','Ventanilla DGI','Ventanilla Finaciera','SV','S'
),
(
'033','20.10.2023 12:02:00','Responsable de Atencion al Usuario','Atencion Usuario','SU','S'
),
(
'034','20.10.2023 12:02:00','Consulta de Im�genes','Atencion Usuario','SU','S'
),
(
'035','20.10.2023 12:02:00','Entrega de Tr�mites Especiales','Atencion Usuario','SU','S'
),
(
'036','20.10.2023 12:02:00','Atencion Personalizada','Atencion Usuario','SU','S'
),
(
'037','20.10.2023 12:02:00','Ventanilla Banpro','Atencion Usuario','SU','S'
),
(
'038','20.10.2023 12:02:00','Asesoria Legal','Atencion Usuario','SU','S'
),
(
'039','20.10.2023 12:02:00','Asistente Despacho','Atencion Usuario','SU','S'
),
(
'040','20.10.2023 12:02:00','Coordinadores','Atencion Usuario','SU','S'
),
(
'041','20.10.2023 12:02:00','Administraci�n','Atencion Usuario','SU','S'
),
(
'042','20.10.2023 12:02:00','Contabilidad','Atencion Usuario','SU','S'
),
(
'043','20.10.2023 12:02:00','Recursos Humanos','Atencion Usuario','SU','S'
)
;

--delete [SCA].[dbo].[CAT_SERVICIO] where COD_SERVICIO='027'


-- procedimiento almacenado para cargar solo los servicios que usaremos
go  
   CREATE PROCEDURE pa_listarAreasIngresar
	as
  SELECT [ID_SERVICIO]
      ,[COD_SERVICIO]
      ,[FECHA_REGISTRO]
      ,[DES_SERVICIO]
      ,[DES_CORTA_SERVICIO]
      ,[SERIE]
      ,[ACTIVO]
  FROM [SCA].[dbo].[CAT_SERVICIO] where ID_SERVICIO>=1033 and ID_SERVICIO<=1049
go

EXECUTE pa_listarAreasIngresar;

-- vamos a crear una tabla controle los campos Consulta, Caso en revisi�n y Citado en ciertos 
-- servicios como son 
--Asistente del Despacho (039), Coordinadores(040), Asesor�a(038) y
--Responsable de atenci�n al usuario (033)
create table CAT_SERVICIODETALLEUSUARIO(
ID_PRIORIDAD int identity(1,1),
DESCRIPCION nvarchar(100) not null,
COD_SERVICIO nvarchar(5) not null
CONSTRAINT fk_detalleservicio_catprioridades 
FOREIGN KEY (COD_SERVICIO) REFERENCES CAT_SERVICIO (COD_SERVICIO)
);
ALTER TABLE CAT_SERVICIOPRIORIDAD
ADD PRIMARY KEY(ID_PRIORIDAD);

select * from CAT_SERVICIODETALLEUSUARIO
-- GUARDANDO DATOS del detalle de cada motivo de la visita

INSERT INTO CAT_SERVICIODETALLEUSUARIO VALUES
('Caso en revisi�n','039'),('Citado','039');

INSERT INTO CAT_SERVICIODETALLEUSUARIO VALUES
('Consulta','040'),('Caso en revisi�n','040'),('Citado','040');

INSERT INTO CAT_SERVICIODETALLEUSUARIO VALUES
('Consulta','038'),('Caso en revisi�n','038'),('Citado','038');

INSERT INTO CAT_SERVICIODETALLEUSUARIO VALUES
('Consulta','033'),('Caso en revisi�n','033'),('Citado','033');

-- actualizando motivos de la ventanilla preferencial
INSERT INTO CAT_SERVICIODETALLEUSUARIO VALUES
('Embarazada','028'),('Persona con ni�o','028'),('Discapacitado','028'),
('Operado','028'),('Tercera edad','028'),('Otros','028');
-- consulta para buscar los motivos de un servicio X
SELECT 
ID_SERVICIO,
ISNULL(CAT_SERVICIO.COD_SERVICIO,'No tiene COD') AS COD_SERV,
ISNULL(ID_PRIORIDAD,0) AS CODIGOTABLA,
CAT_SERVICIO.DES_SERVICIO AS AREAVISITAR,
ISNULL(DESCRIPCION,'No tiene motivos') AS PRIORIDAD_MOTIVO
FROM CAT_SERVICIODETALLEUSUARIO  RIGHT JOIN CAT_SERVICIO  ON 
CAT_SERVICIODETALLEUSUARIO.COD_SERVICIO=CAT_SERVICIO.COD_SERVICIO 
WHERE CAT_SERVICIO.DES_SERVICIO='Ventanilla Preferencial' 
AND CAT_SERVICIO.COD_REGPUBLICO = 'REGISTRO PUB-MANAGUA'

-- ejecuntado el procedimeinto anterior

GO
   ALTER PROCEDURE [dbo].[pa_listarAreasIngresar_serv_espec]
   @descripciondelservicio nvarchar(50),
   @nombreRegistroPublico nvarchar(50)
	as
  SELECT 
ID_SERVICIO,
ISNULL(CAT_SERVICIO.COD_SERVICIO,'No tiene COD') AS COD_SERV,
ISNULL(ID_PRIORIDAD,0) AS CODIGOTABLA,
CAT_SERVICIO.DES_SERVICIO AS AREAVISITAR,
ISNULL(DESCRIPCION,'No tiene motivos') AS PRIORIDAD_MOTIVO
FROM CAT_SERVICIODETALLEUSUARIO  RIGHT JOIN CAT_SERVICIO  ON 
CAT_SERVICIODETALLEUSUARIO.COD_SERVICIO=CAT_SERVICIO.COD_SERVICIO 
WHERE CAT_SERVICIO.DES_SERVICIO=@descripciondelservicio
AND CAT_SERVICIO.COD_REGPUBLICO = @nombreRegistroPublico
-- tareas crear dos procedimientos almacenados 
-- uno para traer todos los servicios en general y otro para cargar los servicios 
--con dependencias
-- cargando todos los servicios del sistema con algunas columnas (completado el 20/10/23)

-- procedimeintos de servicios a nivel general
EXECUTE pa_listarAreasIngresar2

-- procedimeinto que muestra un servicio en especifico con los motivos
   GO
   ALTER PROCEDURE [dbo].[pa_listarAreasIngresar_serv_espec]
   @descripciondelservicio nvarchar(50),
   @nombreRegistroPublico nvarchar(50)
	as
  SELECT 
ID_SERVICIO,
ISNULL(CAT_SERVICIO.COD_SERVICIO,'No tiene COD') AS COD_SERV,
ISNULL(ID_PRIORIDAD,0) AS CODIGOTABLA,
CAT_SERVICIO.DES_SERVICIO AS AREAVISITAR,
ISNULL(DESCRIPCION,'No tiene motivos') AS PRIORIDAD_MOTIVO
FROM CAT_SERVICIODETALLEUSUARIO  RIGHT JOIN CAT_SERVICIO  ON 
CAT_SERVICIODETALLEUSUARIO.COD_SERVICIO=CAT_SERVICIO.COD_SERVICIO 
WHERE CAT_SERVICIO.DES_SERVICIO=@descripciondelservicio
AND CAT_SERVICIO.COD_REGPUBLICO = @nombreRegistroPublico

-- ejecutando el procedimiento almacenado
EXECUTE pa_listarAreasIngresar_serv_espec 'Ventanilla Preferencial','REGISTRO PUB-MANAGUA'
EXECUTE pa_listarAreasIngresar2

--- rearmando el procedimiento almacenado para traer las areas a visitar
SELECT 
ID_SERVICIO,
ISNULL(CAT_SERVICIO.COD_SERVICIO,'No tiene COD') AS COD_SERV,
ISNULL(ID_PRIORIDAD,0) AS CODIGOTABLA,
CAT_SERVICIO.DES_SERVICIO AS AREAVISITAR,
ISNULL(DESCRIPCION,'No tiene motivos') AS PRIORIDAD_MOTIVO
FROM CAT_SERVICIODETALLEUSUARIO  RIGHT JOIN CAT_SERVICIO  ON 
CAT_SERVICIODETALLEUSUARIO.COD_SERVICIO=CAT_SERVICIO.COD_SERVICIO 
WHERE CAT_SERVICIO.COD_REGPUBLICO = 'REGISTRO PUB-MANAGUA'

select * from CAT_SERVICIODETALLEUSUARIO
select * from CAT_SERVICIO

update CAT_SERVICIO set COD_REGPUBLICO = 'REGISTRO PUB-MANAGUA' 
WHERE ID_SERVICIO>=1033 and ID_SERVICIO<=1049


--sp para ver las personas con sus acompa�antes
go
create procedure sp_mostrarvisitantes_acompa�antes
@ID_VISITANTE int
as
 SELECT TBL_NUEVO_VISITANTE.ID_NUEVO_VISITANTE AS ID_VISITANTE, 
 TBL_NUEVO_VISITANTE.IDENTIFICACION AS CED_VISITANTE,
TBL_NUEVO_VISITANTE.NOMBRE AS DATOS_VISITANTE,
TBL_NUEVO_VISITANTE.SEXO AS SEXOVISITANTE,
TBL_NUEVO_VISITANTE.DIRECCION AS DIRECCCION_VISITANTE,
Detalle_Visitante.Identificacion as CED_ACOMPA�ANTE,
Detalle_Visitante.Nombre As ACOMPA�ANTE
FROM Detalle_Visitante INNER JOIN TBL_NUEVO_VISITANTE 
ON Detalle_Visitante.ID_NUEVO_VISITANTE=TBL_NUEVO_VISITANTE.ID_NUEVO_VISITANTE
WHERE Detalle_Visitante.ID_NUEVO_VISITANTE=@ID_VISITANTE;
go

EXECUTE sp_mostrarvisitantes_acompa�antes 12307

select * from Detalle_Visitante

--tarea pendiente generar la tabla de casos prioritarios de las tickes (embarazadas etc