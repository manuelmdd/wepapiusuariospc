

-- procedimiento almacenado que busca el ID DE ticket por el codigo de tk

go 
create procedure pa_buscarTKmaestroXareavisitar
@AREAVISITABUSCAR nvarchar(100)
AS
	SELECT ID_Ticket, COD_TK ,numeracionFinalTK, numeracionInicial, ultimaNumeracion, 
	MAE_Tickets.COD_SERVICIO, CAT_SERVICIO.DES_SERVICIO AS AREAVISITAR 
FROM MAE_Tickets INNER JOIN CAT_SERVICIO
ON MAE_Tickets.COD_SERVICIO=CAT_SERVICIO.COD_SERVICIO
WHERE CAT_SERVICIO.DES_SERVICIO = @AREAVISITABUSCAR
order by ID_Ticket desc

-- ejecuntando procedimeinto

execute pa_buscarTKmaestroXareavisitar 'Tasaci�n'

SELECT * FROM MAE_Tickets

SELECT * FROM DETALLE_Tickets

-- creando procedimiento para guardar el detalle del ticket
go
create procedure pa_guardarTicketDetalle
@id_ticketMaestro int,
@descripcionCODTK nvarchar(50),
@fechaexpiracion datetime,
@estado nvarchar(50),
@ID_nuevoVI int
as
	insert into DETALLE_Tickets(ID_Ticket,DescripCodTK,FechaExpiracion,EstadoTK,ID_NUEVO_VISITANTE)
	values(@id_ticketMaestro,@descripcionCODTK,@fechaexpiracion,@estado,@ID_nuevoVI)
go 



update MAE_Tickets set ultimaNumeracion = ultimaNumeracion +1 where ID_Ticket = 13

-- creando procedimiento para sumarle 1 ala ultima nuemracion de la ticket
go
create procedure sumarenUNoUltimaNumeracion
@id_ticket int
as
update MAE_Tickets set ultimaNumeracion = ultimaNumeracion +1 where ID_Ticket = @id_ticket
--fin sp
EXECUTE sumarenUNoUltimaNumeracion 13



