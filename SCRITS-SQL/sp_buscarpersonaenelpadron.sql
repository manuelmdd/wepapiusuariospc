USE [PJN]
GO
/****** Object:  StoredProcedure [dbo].[BuscarPersona]    Script Date: 13/10/2023 10:37:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[BuscarPersona]
   @cedula varchar(16)
   as
   SELECT [ci] as IDENTIFICACION,
   [NombreCompleto] AS NOMBRE,
   [sexo] AS SEXO,
   [DOMIC] AS DOMICILIO
  FROM [PadronElectoral] where PadronElectoral.ci=@cedula
