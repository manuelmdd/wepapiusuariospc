USE [SCA]
GO
/****** Object:  StoredProcedure [dbo].[LLamandoTk_Atendiendo]    Script Date: 9/1/2024 14:52:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[LLamandoTk_Atendiendo]
@descripcion_codTK nvarchar(50),
@fecha_actual datetime,
@usuario nvarchar(50),
@NombreTerminal nvarchar(50)
as
BEGIN
	BEGIN
		DECLARE @tk_atendiendo nvarchar(50);
		SELECT @tk_atendiendo  = DETALLE_Tickets.DescripCodTK from DETALLE_Tickets where
						DETALLE_Tickets.VENCIMIENTO_TK ='ACTIVA' 
						AND CONVERT(DATE, DETALLE_Tickets.FechaExpiracion) = CONVERT(DATE, @fecha_actual)
						AND DETALLE_Tickets.EstadoTK = 'Atendiendo'
						AND DETALLE_Tickets.DescripCodTK = @descripcion_codTK 
						ORDER BY DETALLE_Tickets.DescripCodTK ASC
		END
	IF @descripcion_codTK = @tk_atendiendo
	BEGIN
		 SELECT 'La ticket esta siendo atendida' AS MENSAJE_ERROR
	END
	ELSE
	
	BEGIN TRY
			
				select top(1) DETALLE_Tickets.DescripCodTK from DETALLE_Tickets where
						DETALLE_Tickets.VENCIMIENTO_TK ='ACTIVA' 
						AND CONVERT(DATE, DETALLE_Tickets.FechaExpiracion) = CONVERT(DATE, @fecha_actual)
						AND DETALLE_Tickets.EstadoTK = 'En Espera' 
						ORDER BY DETALLE_Tickets.FechaExpiracion ASC

						update DETALLE_Tickets SET EstadoTK = 'Atendiendo'
						where DETALLE_Tickets.DescripCodTK =@descripcion_codTK
				and DETALLE_Tickets.VENCIMIENTO_TK ='ACTIVA' 
				AND 
				CONVERT(DATE, DETALLE_Tickets.FechaExpiracion) = CONVERT(DATE, @fecha_actual)

	
					declare @ID_DETALLETK int;
					declare @ID_ESCRITORIO INT;				

					select @ID_DETALLETK = DETALLE_Tickets.ID_Detalle_Ticket FROM
			DETALLE_Tickets where DETALLE_Tickets.VENCIMIENTO_TK ='ACTIVA' 
			AND CONVERT(DATE, DETALLE_Tickets.FechaExpiracion) = CONVERT(DATE, @fecha_actual)
				AND DETALLE_Tickets.DescripCodTK = @descripcion_codTK

				SELECT @ID_ESCRITORIO = EscritoriosDeTrabajo.ID_ESCRITORIO FROM EscritoriosDeTrabajo
				WHERE EscritoriosDeTrabajo.NombreTerminal =@NombreTerminal and 
				EscritoriosDeTrabajo.EstadoTerminal = 'Activa'
			
			
				INSERT INTO DETALLE_ESCRITORIOS(ID_ESCRITORIO, ID_DETALLE_TICKET,
				Usuario) 
				values (@ID_ESCRITORIO, @ID_DETALLETK,@usuario)
	END TRY
	 BEGIN CATCH
        -- Manejo de errores
        SELECT 'La ticket esta vencida' AS MENSAJE_ERROR
            --ERROR_MESSAGE() AS ErrorMessage,
            --ERROR_NUMBER() AS ErrorNumber,
            --ERROR_SEVERITY() AS ErrorSeverity,
            --ERROR_STATE() AS ErrorState;
    END CATCH

END