USE [SCA]
GO
/****** Object:  StoredProcedure [dbo].[pa_AdministrarCatServicios]    Script Date: 8/1/2024 00:53:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[pa_AdministrarCatServicios]

/*
* Indice: Limita la funcion a realizar sobre el catalogo
* 1: Selecionar los estados
* 2: Insertar un nuevo estado
* 3: Editar un estado
* 
* */ 
	@Indice INT,
	@CodServicio NVARCHAR (4),
	@DesServicio NVARCHAR (150),
	@DesCortaServi NVARCHAR (100),
	@Activo NVARCHAR (2),
	@ServicioSerie NVARCHAR(2) = null	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;
	IF (@Indice = 1)
	BEGIN
		SELECT CE.ID_SERVICIO, CE.COD_SERVICIO, CE.DES_SERVICIO, CE.DES_CORTA_SERVICIO, CE.FECHA_REGISTRO,
			CE.SERIE, CASE CE.ACTIVO WHEN 'S' THEN 'ACTIVO' ELSE 'INACTIVO' END AS ACTIVO
		FROM CAT_SERVICIO CE 
	END
	
	IF (@Indice = 2)
	BEGIN
		IF NOT EXISTS (SELECT * FROM CAT_SERVICIO A 
		               WHERE A.DES_SERVICIO = @DesServicio)
			BEGIN				
				IF NOT EXISTS (SELECT * FROM CAT_SERVICIO A 
						   WHERE A.DES_CORTA_SERVICIO = @DesCortaServi )
						   BEGIN						   	
								DECLARE @counter smallint;
								DECLARE @abc AS NVARCHAR (50)
								DECLARE @SERIE AS NVARCHAR (10)
								DECLARE @Index AS INT
								SET @abc = 'BCDFGHJKLMNPQRSTVXYZ'
								--SET @abc = 'AA'
								SET @counter = 1;
								SET @SERIE = ''
								WHILE  @counter < 3
								   BEGIN									  							      
									  --Esta linea determina la posicion de la letra del abecedario aleatoriamente.
									  SET @index = ceiling( RAND() * (len(@abc))) --Uso de la vista para el Rand() y no generar error.
									  SET @SERIE = @SERIE + SUBSTRING(@abc,@Index,1)
									  SET @counter = @counter + 1
									  							            
								   IF (LEN(@SERIE) = 2)
								   BEGIN
									   IF not EXISTS (SELECT cs.SERIE FROM CAT_SERVICIO cs WHERE cs.SERIE IN (@SERIE))
										   BEGIN
												DECLARE @NUMERADOR AS NVARCHAR (4)
												SELECT @NUMERADOR = RIGHT('0000'+CAST(ISNULL(MAX(A.COD_SERVICIO)+1,3) AS NVARCHAR),3) FROM CAT_SERVICIO A
												--PRINT @NUMERADOR 
												INSERT INTO CAT_SERVICIO (COD_SERVICIO,DES_SERVICIO,DES_CORTA_SERVICIO,SERIE,FECHA_REGISTRO,ACTIVO)
												VALUES					 (@NUMERADOR ,@DesServicio ,@DesCortaServi ,@SERIE,GETDATE(),@Activo)
												--PRINT 'insertado'
												--SELECT @SERIE	
												BREAK
										   END	
										ELSE
											SET @counter = 1
											CONTINUE										   	
								   END
								      
								   ELSE
								   	BEGIN
								   		--SET @counter = 1
   										CONTINUE	
								   	END  									
   									
   									END;
   									
   									--PRINT 'Ya Existe esta serie '+@SERIE+''              
								
						   END
						   ELSE
						   	RAISERROR ('La descripcion corta ya existe..!!',16, 1)
			END
		ELSE
			RAISERROR ('Servicio ya existe..!!',16, 1)
		
	END
	
	IF (@Indice = 3)
	BEGIN
		UPDATE CAT_SERVICIO
		SET			
			COD_SERVICIO = @CodServicio,			
			DES_SERVICIO = @DesServicio,
			DES_CORTA_SERVICIO = @DesCortaServi,
			ACTIVO = @Activo,
			SERIE = @ServicioSerie		
		WHERE COD_SERVICIO = @CodServicio
		
	END   
	
END