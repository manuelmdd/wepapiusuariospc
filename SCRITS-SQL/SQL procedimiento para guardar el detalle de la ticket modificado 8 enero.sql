USE [SCA]
GO
/****** Object:  StoredProcedure [dbo].[pa_guardarTicketDetalle]    Script Date: 8/1/2024 00:47:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[pa_guardarTicketDetalle]
@id_ticketMaestro int,
@descripcionCODTK nvarchar(50),
@fechaexpiracion datetime,
@estado nvarchar(50),
@ID_nuevoVI int

as
	insert into DETALLE_Tickets(ID_Ticket,DescripCodTK,FechaExpiracion,EstadoTK,ID_NUEVO_VISITANTE)
	values(@id_ticketMaestro,@descripcionCODTK,@fechaexpiracion,@estado,@ID_nuevoVI)
begin
	DECLARE @ID_TK_DETALLE INT;
	
	SELECT @ID_TK_DETALLE = DETALLE_Tickets.ID_Detalle_Ticket FROM
	DETALLE_Tickets WHERE 
	DETALLE_Tickets.DescripCodTK=@descripcionCODTK and
	DETALLE_Tickets.FechaExpiracion = @fechaexpiracion

	update DETALLE_Tickets set VENCIMIENTO_TK = 'ACTIVA' Where
DETALLE_Tickets.ID_Detalle_Ticket = @ID_TK_DETALLE
END