use SCA

select * from personas

SELECT [ci]
      ,[pnom]
      ,[snom]
      ,[papell]
      ,[sapell]
      ,[sexo]
      ,[domic]
      ,[profesion]
      ,[lug_nac]
      ,[fech_nac]
      ,[cod_dpt]
      ,[cod_mun]
      ,[jrv]
      ,[NombreCompleto]
  FROM [PadronElectoral] where PadronElectoral.ci='4411507940006T'

  CREATE DATABASE PRODEP
  use PRODEP
  -- tabla de pruebas local mi pc
  create table PadronElectoral(
   ci varchar(50)
      ,pnom varchar(50)
      ,snom varchar(50)
      ,papell varchar(50)
      ,sapell varchar(50)
      ,sexo varchar(50)
      ,domic varchar(50)
      ,profesion varchar(50)
      ,lug_nac varchar(50)
      ,fech_nac datetime
      ,cod_dpt int
      ,cod_mun int
      ,jrv int
      ,NombreCompleto varchar(50)
  );

  insert into PadronElectoral values ('4411507940006T','enma','de Jesus','reyes',
  'molina','M','Matagalpa','ing','mat','15.07.1994',1,1,123,'Enmanuel Reyes'
  );
  insert into PadronElectoral values ('4411702960005A','gema','del Rosario','reyes',
  'molina','F','Matagalpa','PRF','mat','17.02.1996',1,1,123,'Gema Reyes'
  ),('44114041979000A','Fatima','del Rosario','Molina',
  'Vasquez','F','Matagalpa','Lic','mat','14.04.1979',1,1,123,'Fatima Molina');
  -- datos guardados el 10 de octubre 2023

  -- listar procedimientos almacenados
  SELECT ROUTINE_NAME FROM INFORMATION_SCHEMA.ROUTINES 
   WHERE ROUTINE_TYPE = 'PROCEDURE'
   ORDER BY ROUTINE_NAME 

   -- creando procedimiento almacenado


--procedimiento almacenado de abajo
USE [PJN]
GO
/****** Object:  StoredProcedure [dbo].[BuscarPersona]    Script Date: 29/11/2023 23:31:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[BuscarPersona]
   @cedula varchar(16)
   as
   SELECT [ci] as IDENTIFICACION,
   [NombreCompleto] AS NOMBRE,
   [sexo] AS SEXO,
   [DOMIC] AS DOMICILIO
  FROM [PadronElectoral] where PadronElectoral.ci=@cedula

-- ejecutando el procedimiento almacenado para buscar las personas por el padron

EXECUTE BuscarPersona '4411507940006T';

use PJN

-- re modificando el sp_buscarpersona
go
ALTER PROCEDURE BuscarPersona
   @cedula varchar(16)
   as
   SELECT [ci]
      ,[pnom]
      ,[snom]
      ,[papell]
      ,[sapell]
      ,[sexo]
      ,[domic]
      ,[profesion]
      ,[lug_nac]
      ,[fech_nac]
      ,[cod_dpt]
      ,[cod_mun]
      ,[jrv]
      ,[NombreCompleto]
  FROM [PadronElectoral] where PadronElectoral.ci=@cedula
go

use SCA

-- cargando datos de tipo de identificacion
SELECT TOP (1000) [VISITANTE_TIPO_ID]
      ,[DESCRIPCION]
      ,[ACTIVO]
      ,[SEXO]
  FROM [SCA].[dbo].[CAT_VISITANTE_TIPO]


  EXECUTE buscarBuscarPersona '4411507940006T';-- es de otra bd

  --para ver estructura del procedimiento almacenado
  EXEC sp_helptext '@pa_BuscarIdentificacionVisita' 

  -- procedimiento �ra encontrar a que bd depende el SP

  go
  SELECT referencing_schema_name, referencing_entity_name, referencing_id, 
  referencing_class_desc, is_caller_dependent
  from sys.dm_sql_referencing_entities('@BuscarPersona','OBJECT')
  go

  -- sp_ para extraer los tipo de identifacion
     -- creando procedimiento almacenado
go  
   CREATE PROCEDURE  sp_TipoIDENTIFICACION
   as
   SELECT  [ID_TIPO_IDENTIFICACION]
      ,[COD_TIPO_IDENTIFICACION]
      ,[DESCRIPCION]
  FROM [SCA].[dbo].[CAT_TIPO_IDENTIFICAION]
go

-- ejecutando 
EXECUTE sp_TipoIDENTIFICACION

-- insertando nuevo visitante
INSERT INTO [TBL_NUEVO_VISITANTE]
           ([COD_PUERTA]
           ,[COD_COMPLEJO]
           ,[COD_TIPO_IDENTIFICACION]
           ,[IDENTIFICACION]
           ,[NOMBRE]
           ,[SEXO]
           ,[DIRECCION])
     VALUES
           ('1','1','01','4411507940006T','Juan Reyes','M',
'Bomba de agua, Matagalpa');

SELECT * FROM DBO.TBL_NUEVO_VISITANTE 

EXECUTE pa_GuardarRegistroNuevoVisitante '1','1','01','4411507940013A','Maria Gomez','M',
'Bomba de agua, Matagalpa'