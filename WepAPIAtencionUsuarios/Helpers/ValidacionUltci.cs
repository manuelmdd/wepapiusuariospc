﻿using System.ComponentModel.DataAnnotations;

namespace WepAPIAtencionUsuarios.Helpers
{
    public class ValidacionUltci : ValidationAttribute
    {
        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return new ValidationResult("El valor de la cedula no puede quedar vacio");
            }
            else if (value is string) 
            {
                string ultimaletra = (string)value;
                //char ultimaletra2 = ultimaletra.LastOrDefault();
                if (ultimaletra != ultimaletra.ToUpper())
                {

                    //return new ValidationResult("",value);
                    return ValidationResult.Success;
                }
            }
            return ValidationResult.Success;
        }
    }
}
