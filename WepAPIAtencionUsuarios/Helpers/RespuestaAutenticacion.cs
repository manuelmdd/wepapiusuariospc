﻿namespace WepAPIAtencionUsuarios.Helpers
{
    public class RespuestaAutenticacion
    {
        public string token { get; set; }
        public DateTime expiracion { get; set; }
    }
}
