﻿using System.ComponentModel.DataAnnotations;

namespace WepAPIAtencionUsuarios.Helpers
{
    public class ChangePass
    {

        [Required(ErrorMessage ="Escriba la contraseña Actual")]
        public string CurrentPassword { get; set; }
        [Required(ErrorMessage ="Escriba la nueva contraseña")]
        [RegularExpression(@"^[a-zA-Z0-9\p{P}]+$", ErrorMessage = "El campo solo puede contener letras, números y símbolos.")]
        [StringLength(maximumLength: 15, ErrorMessage = "El tamaño debe de ser de 11 caracteres")]
        public string NewPassword { get; set; }
    }
}
