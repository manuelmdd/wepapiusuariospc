﻿using System.ComponentModel.DataAnnotations;

namespace WepAPIAtencionUsuarios.Helpers
{
    public class CredencialesUser
    {
        [Required(ErrorMessage = "El correo es Obligatorio")]
        [EmailAddress(ErrorMessage = "La estructura del correo es nombre@ejemplo.com")]
        public string Email { get; set; }
        
        [Required(ErrorMessage ="La contraseña es obligatoria")]
        // este admite espacios en blanco
        //[RegularExpression(@"^[a-zA-Z0-9\s\p{P}]+$", ErrorMessage = "El campo solo puede contener letras, números y símbolos y espacio en blanco.")]

        // este otro no admite espacios en blanco
        [RegularExpression(@"^[a-zA-Z0-9\p{P}]+$", ErrorMessage = "El campo solo puede contener letras, números y símbolos.")]
        [StringLength(maximumLength: 15, ErrorMessage = "El tamaño debe de ser de 11 caracteres")]

        public string Password { get; set; }

    }
}
