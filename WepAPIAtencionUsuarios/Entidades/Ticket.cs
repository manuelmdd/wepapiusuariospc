﻿namespace WepAPIAtencionUsuarios.Entidades
{
    public class Ticket
    {
        // falta validar desde el modelo dataanotations
        public string COD_TK { get; set; }
        public int numeracionFinalTk { get; set; }
        public int numeracionInicial { get; set; }
        public int ultimaNumeracion { get; set; }
       // public DateTime fechaCreacion { get; set; }
        // el user login 

        public string COD_SERVICIO { get; set; }
    }
    public class TicketDevuelto : Ticket
    {
        // falta validar desde el modelo dataanotations

        public int ID_tk { get; set; }
        
        public DateTime fechaCreacion { get; set; }
        // el user login 
        public string userCreator { get; set; }

        public string areaVisitar { get; set; }

    }

    public class ImpresionTicket
    {
        public string AreaVisitar { get; set; }
        public string Turno { get; set;}

        public string Fecha { get; set; }
    }
}
