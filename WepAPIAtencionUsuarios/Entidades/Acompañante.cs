﻿using System.ComponentModel.DataAnnotations;

namespace WepAPIAtencionUsuarios.Entidades
{
    public class Acompañante
    {
        [Required]
        public string identificacionvist { get; set; }

        public string nombrevist { get; set; }
        public string sexovist { get; set; }
        public string direccionvist { get; set; }
    }
}
