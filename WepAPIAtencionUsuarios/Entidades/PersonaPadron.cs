﻿using System.ComponentModel.DataAnnotations;
using WepAPIAtencionUsuarios.Helpers;

namespace WepAPIAtencionUsuarios.Entidades
{
    public class AppResul
    {
        public int Retorno { get; set; }
        public string Mensaje { get; set; }
    }
    public class PersonaPadron : AppResul
    {

        [Required]
        [StringLength(maximumLength: 16)]
      
        public string ci { get; set; }
      
        public string sexo { get; set; }
        public string domic { get; set; }

        [StringLength(210, ErrorMessage = "{0} Debe de Tener una longitud {2} y {1}.", MinimumLength = 6)]
        public string NombreCompleto { get; set; }
        //lista para aca guardar los datos
        public List<PersonaPadron> Acompañantes { get; set; } = new List<PersonaPadron>();
    }


}
