﻿using System.Data;

namespace WepAPIAtencionUsuarios.Entidades
{
    public class areasVisitar
    {
        public int ID_servicio { get; set; }
        public string Cod_Servicio { get; set;}
        public string Des_Servicio { get; set;}

        // lista que recibe los motivos de cada area
        public List<detalleMotivos> detalleMotivos { get; set; } = new List<detalleMotivos>();

       
    }
    public class detalleMotivos
    {
        public int codigotabla { get; set; }
        public string prioridadMotivo { get; set; }
    }
    public class areasBusquedad
    {
        public string nombreArea  { get; set; }
        public string codRegistroPublico { get; set; }
    }
}
