﻿namespace WepAPIAtencionUsuarios.Entidades
{
    public class visitantedcab
    {
        public int id_visitante { get; set; }
        public string ced_visitante { get; set; }
        public string datos_visitante { get; set; }
        public string sexo { get; set; }
        public string direccion { get; set; }

        public List<visitantedevuelto> listadeacompañantes { get; set; } = new List<visitantedevuelto>();
    }
    public class visitantedevuelto
    {
        public string ced_acompañante { get; set; }
        public string datos_acompañante { get; set; }
    }
}
