﻿using System.ComponentModel.DataAnnotations;

namespace WepAPIAtencionUsuarios.Entidades
{
    public class TiposIdentificacion

    {
        [Required]
        
        public int ID_tipoidentificacion { get; set; }
        public string Cod_tipo_identificacion { get; set; }

        public string Descripcion { get; set; }
    }
}
