﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace WepAPIAtencionUsuarios.Entidades
{
    
    public class NuevoVisitante
    {
        
        public int cod_puerta { get; set; }
        public string cod_complejo { get; set; }
           
        public string id_tipo_identificacion { get; set; }
    
        public string identificacion { get; set; }
        public  string nombrev { get; set; }
        public string sexov { get; set; }
        public string direccionv { get; set; }

        // lista para guardar el acompañante
        public List<Acompañante> Acompañante { get; set; }

    }
   
}
