﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using WepAPIAtencionUsuarios.Helpers;

namespace WepAPIAtencionUsuarios.Controllers
{
    [ApiController]
    [Route("api/cuentas")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    //con este atributo autenticamos todo este controlador
    public class CuentasController : ControllerBase
    {
        private readonly UserManager<IdentityUser> usermanager;

        private readonly string? q;
        private readonly IConfiguration configuration;
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly IAuthenticationService _authenticationService;

        public CuentasController(UserManager<IdentityUser> usermanager, IConfiguration configuration,
            SignInManager<IdentityUser> signInManager, IAuthenticationService authenticationService)
        {
            this.usermanager = usermanager;
            this.configuration = configuration;
            this.signInManager = signInManager;
            this.q = configuration["llavejwt"];
            _authenticationService = authenticationService;

            if (q == null)
            {
                q = "cadena nula de q";
                BadRequest("Cadena nula");
            }
            else if (q == "")
            {
                q = "cadena vacia";
                BadRequest("Cadena vacia");
            }
           
        }

        [HttpPost("registrar")]
        [AllowAnonymous]
        public async Task<ActionResult<RespuestaAutenticacion>> Registrar(CredencialesUser usuarioenv)
        {
           
                var usuarioi = new IdentityUser { UserName = usuarioenv.Email, Email = usuarioenv.Email };
                var resultado = await usermanager.CreateAsync(usuarioi, usuarioenv.Password);
                if (resultado.Succeeded)
                {
                    return await construirToken(usuarioenv);
                }
                else
                {
                    return BadRequest(resultado.Errors);
                }
                    
        }
        //login
        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<ActionResult<RespuestaAutenticacion>> Login(CredencialesUser credeusuario)
        {
            var resultado = await signInManager.PasswordSignInAsync(credeusuario.Email, credeusuario.Password,
                isPersistent: false, lockoutOnFailure:false);

            if (resultado.Succeeded)
            {
                return await construirToken(credeusuario);
            }
            else 
            {
                return BadRequest("Login incorrecto");
            }
        }

        //cerrar sesion
        [HttpPost("logout")]
        public async Task<ActionResult> CerrarSesion()
        {
            await signInManager.SignOutAsync();

            //invalidando token
            await HttpContext.SignOutAsync();

            await _authenticationService.SignOutAsync(HttpContext,null,null);
            return Ok(new { message = "La sesion se ha cerrado exitosamente" });

            //// Cerrar sesión con Identity
            //await signInManager.SignOutAsync();

            //// Obtener el contexto de la solicitud HTTP
            //var httpContext = HttpContext;

            //// Invalidar el token actual forzando la expiración
            //await _authenticationService.SignOutAsync(httpContext, "Bearer", null);
            ////await _authenticationService.SignOutAsync(httpContext, "JWTBearer", null);


            //return Ok(new { message = "Sesion cerrada con Éxito" });
        }

        // actualizar contraseña (esta dando error favor buscarle reparar) se debe seguir probando
        [HttpPost("Changepassword")]
        public async Task<ActionResult> cambiarcontraseña([FromBody] ChangePass contrnueva)
        {
            //var user = await usermanager.FindByNameAsync(contrnueva.usercorreo);
            //var user = await usermanager.FindByNameAsync(User.Identity.Name);

            var emailclaims = HttpContext.User.Claims.Where(claim => claim.Type == "email").FirstOrDefault();
            var email = emailclaims.Value;

            var user = await usermanager.FindByNameAsync(email);

            if (user == null)
            {
                return NotFound(new { message = "Usuario no encontrado"});
            }

            var changepasswordResult = await usermanager.ChangePasswordAsync(user, contrnueva.CurrentPassword, contrnueva.NewPassword);
            if (changepasswordResult.Succeeded)
            {
                //cerramos sesion 
                await signInManager.SignOutAsync();
                await signInManager.SignInAsync(user, isPersistent: false);
                return Ok(new { message = "Contraseña cambiada exitosamente." });
            }
            else 
            {
                return BadRequest(new { message = "Error al cambiar la contraseña.", errors = changepasswordResult.Errors });
            }
        }

        // ENDPOINT para renovar token
        [HttpGet("RenovarToken")]
        [AllowAnonymous]
        public async Task<ActionResult<RespuestaAutenticacion>> Renovar()
        {
            var emailclaims = HttpContext.User.Claims.Where(claim => claim.Type == "email").FirstOrDefault();
            var email = emailclaims.Value;
            CredencialesUser credencialesUsuario = new CredencialesUser()
            {
                Email = email 
            };
            return await construirToken(credencialesUsuario);
        }
        private async Task<RespuestaAutenticacion> construirToken(CredencialesUser credencialUsuario)
        {
            var claims = new List<Claim>()
            {
                new Claim("email", credencialUsuario.Email)
            };

            var usuario = await usermanager.FindByEmailAsync(credencialUsuario.Email);
            var claimSBD = await usermanager.GetClaimsAsync(usuario);

            claims.AddRange(claimSBD);

            var llave = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["llavejwt"]));

            //var llave = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(q));

            var creds = new SigningCredentials(llave, SecurityAlgorithms.HmacSha256);
            
            var expiracion = DateTime.UtcNow.AddHours(2);// hemos dado una hora de validacion del token

            var securityToken = new JwtSecurityToken(issuer: null, audience: null, claims: claims,
                expires: expiracion, signingCredentials: creds);

            return new RespuestaAutenticacion()
            {
                token = new JwtSecurityTokenHandler().WriteToken(securityToken),
                expiracion = expiracion
            };

        }

    }
}
