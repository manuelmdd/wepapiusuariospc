﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WepAPIAtencionUsuarios.Entidades;
using WepAPIAtencionUsuarios.Interfaces;

namespace WepAPIAtencionUsuarios.Controllers
{
    [ApiController]
    [Route("api/areasV")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    //con este atributo autenticamos todo este controlador
    public class AreasMotivosController : ControllerBase
    {
        public AppResul nuevoResultadoError;
        private readonly IVisitante visitante;
        private readonly IProcedimientosSp procedimiento;

        public AreasMotivosController(IVisitante visitante, IProcedimientosSp procedimientos)
        {
            nuevoResultadoError = new AppResul();
            this.visitante = visitante;
            this.procedimiento = procedimientos;
        }




        [HttpGet("areasvisitaManagua")]
        [ResponseCache(Duration = 10)]
        public async Task<ActionResult> getareas()
        {
            try
            {
                var areasencontradas = await procedimiento.Sp_Areas_Mostrar();
                if (areasencontradas.Count > 0)
                {
                    return Ok(areasencontradas);
                }
                else
                {
                    nuevoResultadoError.Retorno = 1;
                    nuevoResultadoError.Mensaje = "No se encontraron Areas";
                    return BadRequest(nuevoResultadoError);
                }
            }
            catch (Exception)
            {
                nuevoResultadoError.Retorno = 1;
                nuevoResultadoError.Mensaje = "Ocurrio un error al enviar los datos en la base de datos";
                return NotFound(nuevoResultadoError);
            }
        }
        // este endpoint se debera corregir para dar salida al json


        [HttpGet("areaEspecifica/{nombreArea}/{codRegistroPub}/result")]
        public async Task<ActionResult> getsubareas(string nombreArea, string codRegistroPub)
        {
            try
            {
                var resultadosubareas = await procedimiento.Sp_detalleAreas(nombreArea, codRegistroPub);
                if (resultadosubareas.Count == 0)
                {
                    nuevoResultadoError.Retorno = 1;
                    nuevoResultadoError.Mensaje = "El area: " + nombreArea + " no posee sub areas, favor revisar el codigo " + codRegistroPub + "";
                    return BadRequest(nuevoResultadoError);
                }
                else
                {
                    return Ok(resultadosubareas);
                }
            }
            catch (Exception)
            {
                nuevoResultadoError.Retorno = 1;
                nuevoResultadoError.Mensaje = "Al enviar los siguientes: " + nombreArea + ", " + codRegistroPub + " ocurrio un error con la Base de Datos";
                return NotFound(nuevoResultadoError);
            }


        }

        //// post para crear nueva area
        //[HttpPost("nuevaArea")]
        //public ActionResult NuevaArea(areasVisitar nuevaarea)
        //{
        //    return Ok();
        //}

        //[HttpPatch("actualizarArea/{nombrearea}/update")]
        //public ActionResult Updatearea(string nombrearea, [FromBody] areasVisitar areaupdate)
        //{
            
        //    return Ok();
        //}
    }
}
