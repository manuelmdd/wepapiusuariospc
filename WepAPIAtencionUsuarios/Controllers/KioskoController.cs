﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System.Formats.Asn1;
using WepAPIAtencionUsuarios.Entidades;
using WepAPIAtencionUsuarios.Interfaces;

namespace WepAPIAtencionUsuarios.Controllers
{
    [ApiController]
    [Route("api/Kiosko")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    //con este atributo autenticamos todo este controlador
    public class KioskoController : ControllerBase
    {
        private readonly IVisitante visitante;
        private readonly IProcedimientosSp procedimiento;
        public AppResul nuevoResultadoError;
        public KioskoController(IVisitante visitante, IProcedimientosSp procedimientos)
        {
            this.visitante = visitante;
            this.procedimiento = procedimientos;
            nuevoResultadoError = new AppResul();
        }

        [HttpGet("vervisit/{idvisitante:int}", Name = "insertvisit")]
        
        public async Task<ActionResult> getvistantes(int idvisitante)
        {
            try
            {
                var resultado = await visitante.Mostrarvistante(idvisitante);

                if (resultado.id_visitante == 0)
                {
                    nuevoResultadoError.Retorno = 1;
                    nuevoResultadoError.Mensaje = "El ID " + idvisitante + " no corresponde a ningun visitante registrado";
                    return BadRequest(nuevoResultadoError);
                }
                else
                {
                    return Ok(resultado);
                }
            }
            catch (SqlException)
            {
               
                nuevoResultadoError.Retorno = 1;
                nuevoResultadoError.Mensaje = "Ocurrio un error al conectarse con la BD";
                return NotFound(nuevoResultadoError);
            }
            
            
        }

        [HttpPost("newvisitant")]
        public async Task<ActionResult> postvist(NuevoVisitante visitante1)
        {
            try
            {
                var resultado = await visitante.InsertNuevovistante(visitante1);

                if (await visitante.verificarAcompañante(resultado, visitante1) == true)
                {
                    //return Ok(resultado);
                    // return CreatedAtRoute("newvistant",new { codPuerta = visitante1.cod_puerta},resultado);

                    return CreatedAtRoute("insertvisit", new { idvisitante = resultado }, visitante1);
                }
                else
                {
                    nuevoResultadoError.Retorno = 1;
                    nuevoResultadoError.Mensaje = "Ocurrio un error al guardar el acompañante al conectarse con la BD";
                    return BadRequest(nuevoResultadoError);
                }

            }
            catch (Exception)
            {
                nuevoResultadoError.Retorno = 1;
                nuevoResultadoError.Mensaje = "Ocurrio un error al enviar los datos en la base de datos";
                return NotFound(nuevoResultadoError);
            }

        }
      
    }
}
