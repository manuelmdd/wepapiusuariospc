﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System.Net;
using System.Net.Mime;
using System.Text.Json;
using WepAPIAtencionUsuarios.Entidades;
using WepAPIAtencionUsuarios.Interfaces;

namespace WepAPIAtencionUsuarios.Controllers
{
    [ApiController]
    [Route("api/personas")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    //con este atributo autenticamos todo este controlador
    public class PersonaPadronController : ControllerBase
    {
        private readonly IFunciones funciones;
        public AppResul nuevoResultadoError;
        public PersonaPadronController(IFunciones funciones)
        {
            this.funciones = funciones; 
            nuevoResultadoError = new AppResul();
        }

        [HttpGet("cargartipoidentificacion")]
        [ResponseCache(Duration = 10)]
        public async Task<ActionResult<List<TiposIdentificacion>>> GetlistaPersonas2()
        {
            try
            {
                var resultado = await funciones.cargarTipoIdentificacion();
                return Ok(resultado);
            }
            catch (Exception)
            {
                nuevoResultadoError.Retorno = 1;
                nuevoResultadoError.Mensaje = "Ocurrio un error al conectarse con la BD";
                return NotFound(nuevoResultadoError);
            }


        }

        [HttpGet("search/{ci}/result")]
        public async Task<ActionResult<List<PersonaPadron>>> GetPersonasearch(string ci)
        {
            try
            {
               var resultado = await funciones.Buscarxceduda(ci);
                if (resultado.Count()==0)
                {
                    nuevoResultadoError.Retorno = 1;
                    nuevoResultadoError.Mensaje = "No se encontro el visitante: "+ci+", verifique el número de cédula";
                    return BadRequest(nuevoResultadoError);
                }
                else
                {
                    //return resultado;
                    return Ok(resultado);
                }

            }
            catch (SqlException)
            {
                
                nuevoResultadoError.Retorno = 1;
                nuevoResultadoError.Mensaje = "Ocurrio un error en la conexión con la base de datos";
                return NotFound(nuevoResultadoError);
            }

        }



    }
}
