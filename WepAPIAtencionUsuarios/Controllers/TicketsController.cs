﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using WepAPIAtencionUsuarios.Entidades;
using WepAPIAtencionUsuarios.Interfaces;

namespace WepAPIAtencionUsuarios.Controllers
{
    [ApiController]
    [Route("api/Tickets")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class TicketsController : ControllerBase
    {
        public AppResul nuevoResultadoError;
        private readonly IFuncionesTickets _funcionesTickets;

        public TicketsController(IFuncionesTickets funcionesTickets)
        {
            nuevoResultadoError = new AppResul();
            _funcionesTickets = funcionesTickets;
        }

        [HttpGet("verTKmaestro/{idTk:int}", Name = "verMaestroTK")]
        public async Task<ActionResult<TicketDevuelto>> vertkMaestro(int idTk)
        {
            try
            {
                var resultado = await _funcionesTickets.mostrarTicketMaestro(idTk);

                if (resultado.ID_tk == 0)
                {
                    nuevoResultadoError.Retorno = 1;
                    nuevoResultadoError.Mensaje = "El ID " + idTk + " no corresponde a ninguna TICKET GUARDADA";
                    return NotFound(nuevoResultadoError);
                }
                else
                {
                    return Ok(resultado);
                }
            }
            catch (SqlException)
            {

                nuevoResultadoError.Retorno = 1;
                nuevoResultadoError.Mensaje = "Ocurrio un error al conectarse con la BD";
                return BadRequest(nuevoResultadoError);
            }
        }



        [HttpPost("nuevaTicketMaestro")]
        public async Task<ActionResult<Ticket>> nuevaticketxServicio(Ticket ticket)
        {
            var emailclaims = HttpContext.User.Claims.Where(claim => claim.Type == "email").FirstOrDefault();
            var email = emailclaims.Value.ToString();

            try
            {
                int id_tk = await _funcionesTickets.nuevaTicketMaestro(ticket, email);

                if (id_tk > 0)
                {
                    TicketDevuelto respuestaObjeto = await _funcionesTickets.mostrarTicketMaestro(id_tk);
                    return CreatedAtRoute("verMaestroTK", new { idTK = id_tk }, respuestaObjeto);
                }
                else
                {
                    nuevoResultadoError.Retorno = 1;
                    nuevoResultadoError.Mensaje = "Ocurrio un error al ingresar la ticket, FAVOR REVISE EL CODIGO DE SERVICIO " + ticket.COD_SERVICIO + "";
                    return NotFound(nuevoResultadoError);
                }

            }
            catch (Exception)
            {
                nuevoResultadoError.Retorno = 1;
                nuevoResultadoError.Mensaje = "Ocurrio un error al conectarse a la BD";
                return BadRequest(nuevoResultadoError);
            }
        }

        // metodo para imprimir la ticket
        [HttpPost("ImpresionTK")]
        public async Task<ActionResult<ImpresionTicket>> imprimirTK(string areaVisitar, int ID_visitante)
        {
            try
            {
                ImpresionTicket objetoRespuesta = await _funcionesTickets.impresionTicketDetalle(areaVisitar, ID_visitante);
                if (objetoRespuesta.Turno == "0")
                {
                    return NotFound(objetoRespuesta);
                }
                else
                {
                    return Ok(objetoRespuesta);
                }


            }
            catch (Exception)
            {
                nuevoResultadoError.Retorno = 1;
                nuevoResultadoError.Mensaje = "Ocurrio un error al conectarse a la BD";
                return BadRequest(nuevoResultadoError);
            }
        }
        // metodo para llamar la ticket
        [HttpPost("Llamando_Tk")]
        public async Task<ActionResult<string>> llamandoTk(string cod_tk, string nombreComputadoraTerminal)
        {
            try
            {
                var emailclaims = HttpContext.User.Claims.Where(claim => claim.Type == "email").FirstOrDefault();
                var UserEmail = emailclaims.Value.ToString();

                string respuesta = await _funcionesTickets.llamado_ticket(cod_tk,UserEmail,nombreComputadoraTerminal);
                if (cod_tk.Equals(respuesta))
                {
                    nuevoResultadoError.Retorno = 0;
                    nuevoResultadoError.Mensaje =  "LLAMANDO USUARIO CON TICKET N°: " + respuesta + "";
                    
                    return Ok(nuevoResultadoError);
                }
                else
                {
                    nuevoResultadoError.Retorno = 1;
                    nuevoResultadoError.Mensaje = respuesta;
                    return BadRequest(nuevoResultadoError);
                }
              

            }
            catch (Exception)
            {
                nuevoResultadoError.Retorno = 1;
                nuevoResultadoError.Mensaje = "Ocurrio un error al conectarse a la BD";
                return NotFound(nuevoResultadoError);
            }
        }
        //metodos post para los llamados de las tk
        [HttpPost("Ticket-Atendido")]
        public async Task<ActionResult<string>> marcar_atendido(string cod_tk)
        {
            try
            {
                DateTime fecha_actual = DateTime.Now.Date;
                bool bandera = await _funcionesTickets.marcar_tkAtendido(cod_tk);
                if (bandera == true) 
                {
                    string mensaje = "La ticket: " + cod_tk + " FUE ATENDIDA CON EXITO";
                    nuevoResultadoError.Retorno = 0;
                    nuevoResultadoError.Mensaje = mensaje;
                    return Ok(nuevoResultadoError);
                }
                else
                {
                    string mensaje = "La ticket: " + cod_tk + " no ha sido atendida, favor revise";
                    nuevoResultadoError.Retorno = 1;
                    nuevoResultadoError.Mensaje = mensaje;
                    return BadRequest(nuevoResultadoError);
                }
            }
            catch (Exception)
            {
                nuevoResultadoError.Retorno = 1;
                nuevoResultadoError.Mensaje = "Ocurrio un error al conectarse a la BD";
                return NotFound(nuevoResultadoError);
            }
        }

        [HttpPost("Marcar-TKretirada")]
        public async Task<ActionResult<string>> marcarRetiro(string cod_tk)
        {
            try
            {
                
                bool bandera = await _funcionesTickets.marcar_tkSE_RETIRO(cod_tk);
                if (bandera == true)
                {
                    string mensaje = "La ticket: " + cod_tk + " FUE MARCADA COMO RETIRO CON EXITO";
                    nuevoResultadoError.Retorno = 0;
                    nuevoResultadoError.Mensaje = mensaje;
                    return Ok(nuevoResultadoError);
                }
                else
                {
                    string mensaje = "La ticket: " + cod_tk + " no ha sido retirada, favor revise";
                    nuevoResultadoError.Retorno = 1;
                    nuevoResultadoError.Mensaje = mensaje;
                    return BadRequest(nuevoResultadoError);
                }
            }
            catch (Exception)
            {
                nuevoResultadoError.Retorno = 1;
                nuevoResultadoError.Mensaje = "Ocurrio un error al conectarse a la BD";
                return NotFound(nuevoResultadoError);
            }
        }

        [HttpPost("Marcar-Volver-Llamar")]
        public async Task<ActionResult<string>> marcarRellamada(string cod_tk)
        {
            try
            {

                bool bandera = await _funcionesTickets.marcar_tkVOLVER_ALLAMAR(cod_tk);
                if (bandera == true)
                {
                    string mensaje = "La ticket: " + cod_tk + " FUE MARCADA COMO EN ESPERA";
                    nuevoResultadoError.Retorno = 0;
                    nuevoResultadoError.Mensaje = mensaje;
                    return Ok(nuevoResultadoError);
                }
                else
                {
                    string mensaje = "La ticket: " + cod_tk + " no ha sido marcada, favor revise";
                    nuevoResultadoError.Retorno = 1;
                    nuevoResultadoError.Mensaje = mensaje;
                    return BadRequest(nuevoResultadoError);
                }
            }
            catch (Exception)
            {
                nuevoResultadoError.Retorno = 1;
                nuevoResultadoError.Mensaje = "Ocurrio un error al conectarse a la BD";
                return NotFound(nuevoResultadoError);
            }
        }
    }
}
