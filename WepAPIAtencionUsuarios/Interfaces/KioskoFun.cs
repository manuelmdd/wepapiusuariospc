﻿using WepAPIAtencionUsuarios.Entidades;
using Microsoft.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using System.Reflection.PortableExecutable;

namespace WepAPIAtencionUsuarios.Interfaces
{
    public class KioskoFun : IFunciones
    {
        private readonly string? _con1;
        private readonly string? _con2;
        public KioskoFun(IConfiguration configuracion) 
        {
            this._con1 = configuracion.GetConnectionString("ConSBD1");
            this._con2 = configuracion.GetConnectionString("ConSCA1");
        } 

        public async Task<List<PersonaPadron>> Buscarxceduda(string cedula)
        {
            List<PersonaPadron> personas = new List<PersonaPadron>();
            //PersonaPadron personal2 = new PersonaPadron();

            ContentResult contenido = new ContentResult();
            
                using (SqlConnection conexbd = new SqlConnection(_con1))
                {
                    
                    await conexbd.OpenAsync();
                    SqlCommand cmd = new("BuscarPersona", conexbd);
                    cmd.CommandTimeout = 20;//segundos 
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add("@cedula", System.Data.SqlDbType.VarChar, 16).Value = cedula;

                    using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            PersonaPadron personalista = new PersonaPadron
                            {
                                ci = reader.GetString(0),

                                NombreCompleto = reader.GetString(1),
                                sexo = reader.GetString(2),
                                domic = reader.GetString(3)
                            };

                            personalista.Retorno = 0;
                            personalista.Mensaje = "Cargado con exito";


                            personas.Add(personalista);
                        }

                        await reader.CloseAsync();
                        await conexbd.CloseAsync();
                    }

                }
                contenido.ContentType = "application/json;charset=utf-8";
                contenido.StatusCode = 200;
                var mijson = JsonSerializer.Serialize(personas);
                contenido.Content = mijson;
                //return contenido;
                return personas;
           
                
            
        }

        public async Task<List<TiposIdentificacion>> cargarTipoIdentificacion()
        {
            List<TiposIdentificacion> listmostrar = new List<TiposIdentificacion>();
            using (SqlConnection conexionbd = new SqlConnection(_con2))
            {
                await conexionbd.OpenAsync();
                SqlCommand cmd = new("sp_TipoIDENTIFICACION", conexionbd);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        TiposIdentificacion areasVisita = new TiposIdentificacion
                        {
                            ID_tipoidentificacion = reader.GetInt32(0),
                            Cod_tipo_identificacion = reader.GetString(1),
                            Descripcion = reader.GetString(2)
                       };

                      listmostrar.Add(areasVisita);
                    }

                    await reader.CloseAsync();
                    await conexionbd.CloseAsync();
                }

            }
            return listmostrar;
        }
    }
}
