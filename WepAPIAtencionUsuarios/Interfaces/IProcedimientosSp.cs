﻿using WepAPIAtencionUsuarios.Entidades;

namespace WepAPIAtencionUsuarios.Interfaces
{
    public interface IProcedimientosSp
    {
        Task<List<areasVisitar>> Sp_Areas_Mostrar();

        Task<List<areasVisitar>> Sp_detalleAreas(string nombrearea, string codRegistroPub);
        
    }
}
