﻿using WepAPIAtencionUsuarios.Entidades;
using Microsoft.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using Microsoft.Extensions.Configuration;

namespace WepAPIAtencionUsuarios.Interfaces
{
    public class NuevoVisitanteFun : IVisitante
    {
        private readonly string? _con1;
        private readonly string? _con2;
        public NuevoVisitanteFun(IConfiguration configuracion)
        {
            this._con1 = configuracion.GetConnectionString("ConSBD1");
            this._con2 = configuracion.GetConnectionString("ConSCA1");
        }

        public async Task<int> InsertNuevovistante(NuevoVisitante personanueva)
        {
            List<int> ID_CODIGOVISITANTE = new List<int>();
            int codigobdretur = 0;
            using (SqlConnection con = new SqlConnection(_con2))
            {
                await con.OpenAsync();
                SqlCommand cmd = new("pa_GuardarRegistroNuevoVisitante", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@COD_COMPLEJO",personanueva.cod_complejo);
                cmd.Parameters.AddWithValue("@COD_PUERTA", personanueva.cod_puerta);
                cmd.Parameters.AddWithValue("@COD_TIPO_IDENTIFICACION", personanueva.id_tipo_identificacion);
                cmd.Parameters.AddWithValue("@IDENTIFICACION", personanueva.identificacion);
                cmd.Parameters.AddWithValue("@NOMBRE", personanueva.nombrev);
                cmd.Parameters.AddWithValue("@SEXO", personanueva.sexov);
                cmd.Parameters.AddWithValue("@DIRECCION", personanueva.direccionv);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {

                   // segunda prueba
                     if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            codigobdretur = reader.GetInt32(0);
                            ID_CODIGOVISITANTE.Add(codigobdretur);
                        }
                        await reader.CloseAsync();
                        await con.CloseAsync();

                    }
                    else
                    {
                        await reader.CloseAsync();
                        await con.CloseAsync();
                    }

                }
            }
            //return codigobdretur;
            return ID_CODIGOVISITANTE.FirstOrDefault();
        }

        public async Task<visitantedcab> Mostrarvistante(int id_visitante)
        {
            visitantedevuelto listaacompañantes = new visitantedevuelto();
            visitantedcab nuevoobje = new visitantedcab();
            bool contador = true;

            using (SqlConnection con = new SqlConnection(_con2))
            {
                await con.OpenAsync();
                SqlCommand cmd = new("sp_mostrarvisitantes_acompañantes", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_VISITANTE", id_visitante);
                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    if (reader.HasRows)
                    {
                        while (await reader.ReadAsync())
                        {
                            
                            listaacompañantes.ced_acompañante = reader.GetString(5);
                            listaacompañantes.datos_acompañante = reader.GetString(6);

                            nuevoobje.listadeacompañantes.Add(listaacompañantes);

                            
                            if (contador == true)
                            {
                                nuevoobje.id_visitante = reader.GetInt32(0);
                                nuevoobje.ced_visitante = reader.GetString(1);
                                nuevoobje.datos_visitante = reader.GetString(2);
                                nuevoobje.sexo = reader.GetString(3);
                                nuevoobje.direccion = reader.GetString(4);
                            }
                            contador = false;
                        }

                        await reader.CloseAsync();
                        await con.CloseAsync();
                       
                    }
                    else 
                    {
                        await reader.CloseAsync();
                        await con.CloseAsync();
                      
                    }
                    
                    
                }
            }
            return nuevoobje;
        }

        public async Task<bool> verificarAcompañante(int id_codvisitante, NuevoVisitante personanueva)
        {
            using (SqlConnection con = new SqlConnection(_con2))
            {
                try//validando que no se pierda la conexion 
                {
                    await con.OpenAsync();
                    //llenado los parametros
                    foreach (var item in personanueva.Acompañante)
                    {

                        SqlCommand cmd = new("pa_GuardarDetalleNuevoVisitante", con);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ID_NUEVO_VISITANTE", id_codvisitante);
                        cmd.Parameters.AddWithValue("@IDENTIFICACION", item.identificacionvist);
                        cmd.Parameters.AddWithValue("@NOMBRE", item.nombrevist);
                        cmd.Parameters.AddWithValue("@SEXO", item.sexovist);
                        cmd.Parameters.AddWithValue("@DIRECCION", item.direccionvist);
                        await cmd.ExecuteNonQueryAsync();
                    }
                    bool bandera = true;
                    await con.CloseAsync();
                    return bandera;
                }
                catch (Exception)
                {
                    bool bandera = false;
                    await con.CloseAsync();
                    return bandera;
                }
               
            }
        }

    }
}
