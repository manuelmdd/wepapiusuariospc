﻿using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System.Data;
using System.Globalization;
using System.Runtime.Intrinsics.X86;
using WepAPIAtencionUsuarios.Entidades;


namespace WepAPIAtencionUsuarios.Interfaces
{
    public class implementacionClassTickets : IFuncionesTickets
    {
        private readonly string? _con2;
        private readonly IVisitante funcionesVisitante;
        public implementacionClassTickets(IConfiguration configuracion, IVisitante funcionesvisitante)
        {
            _con2 = configuracion.GetConnectionString("ConSCA1");
            funcionesVisitante = funcionesvisitante;
        }

        public async Task<TicketDevuelto> mostrarTicketMaestro(int id_ticket)
        {
            DataTable tablaDatos = await cargardatosUnticketMaestro(id_ticket);

            TicketDevuelto ticketMaestro = new TicketDevuelto();

            for (int i = 0; i < tablaDatos.Rows.Count; i++)
            {
                ticketMaestro.ID_tk = Convert.ToInt32(tablaDatos.Rows[i]["ID_Ticket"]);
                ticketMaestro.COD_TK = Convert.ToString(tablaDatos.Rows[i]["COD_TK"]);
                ticketMaestro.numeracionFinalTk = Convert.ToInt32(tablaDatos.Rows[i]["numeracionFinalTK"]);
                ticketMaestro.numeracionInicial = Convert.ToInt32(tablaDatos.Rows[i]["numeracionInicial"]);
                ticketMaestro.ultimaNumeracion = Convert.ToInt32(tablaDatos.Rows[i]["ultimaNumeracion"]);
                ticketMaestro.fechaCreacion = Convert.ToDateTime(tablaDatos.Rows[i]["fechaCreacion"]);
                ticketMaestro.userCreator = tablaDatos.Rows[i]["userLogin"].ToString();
                ticketMaestro.COD_SERVICIO = tablaDatos.Rows[i]["COD_SERVICIO"].ToString();
                ticketMaestro.areaVisitar = Convert.ToString(tablaDatos.Rows[i]["AREAVISITAR"]);
            }
            return ticketMaestro;
        }

        public async Task<int> nuevaTicketMaestro(Ticket ticket, string emailUser)
        {
            List<int> ListaID_TICKET_MAESTRO = new List<int>();
            int ID_TICKET_MAESTRO = 0;
            using (SqlConnection con = new SqlConnection(_con2))
            {
                await con.OpenAsync();
                SqlCommand cmd = new("pa_nuevaTicketsRegistroPub", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@COD_TK", ticket.COD_TK);
                cmd.Parameters.AddWithValue("@numeracionFinalTK", ticket.numeracionFinalTk);
                cmd.Parameters.AddWithValue("@numeracionInicial", ticket.numeracionInicial);
                cmd.Parameters.AddWithValue("@ultimaNumeracion", ticket.ultimaNumeracion);
                cmd.Parameters.AddWithValue("@fechaCreacion", DateTime.Now);
                cmd.Parameters.AddWithValue("@userLogin", emailUser);


                if (await comprobarSiexisteelCodServ(ticket.COD_SERVICIO) == true)
                {
                    cmd.Parameters.AddWithValue("@COD_SERVICIO", ticket.COD_SERVICIO);
                    using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (reader.Read())
                        {
                            ID_TICKET_MAESTRO = reader.GetInt32(0);
                            ListaID_TICKET_MAESTRO.Add(ID_TICKET_MAESTRO);
                        }
                    }
                    return ListaID_TICKET_MAESTRO.FirstOrDefault();
                }
                else
                {
                    ListaID_TICKET_MAESTRO.Add(ID_TICKET_MAESTRO);
                    return ListaID_TICKET_MAESTRO.FirstOrDefault();
                }

                
            }
        }

      

      

        public async Task<DataTable> cargardatosUnticketMaestro(int ID_parametro)
        {
            DataTable tabladeDatos = new DataTable();
            using (SqlConnection con = new SqlConnection(_con2))
            {
                await con.OpenAsync();
                SqlCommand cmd = new("pa_mostrarticket_MAESTRO", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_TK", ID_parametro);
                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    tabladeDatos.Load(reader);
                }
                return tabladeDatos;
            }
        }

        public async Task<ImpresionTicket> impresionTicketDetalle(string areaVisitar, int ID_visitante)
        {
            DataTable tabladeDatos = new DataTable();
            ImpresionTicket objetoIMPRIMIR = new ImpresionTicket();
            using (SqlConnection con = new SqlConnection(_con2))
            {
                await con.OpenAsync();
                SqlCommand cmd = new("pa_buscarTKmaestroXareavisitar", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@AREAVISITABUSCAR", areaVisitar);
                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    tabladeDatos.Load(reader);
                    visitantedcab ComproVisitante = new visitantedcab();

                    ComproVisitante = await funcionesVisitante.Mostrarvistante(ID_visitante);

                    if (tabladeDatos.Rows.Count == 0 || ComproVisitante.id_visitante==0)
                    {
                        objetoIMPRIMIR.Turno = "0";
                        objetoIMPRIMIR.AreaVisitar = "NO se pudo imprimir el Ticket, Favor revise el nombre del área: "+areaVisitar+" y el codigo del visitante: "+ID_visitante+"";
                        return objetoIMPRIMIR;
                    }
                    else 
                    {

                        SqlCommand cmd2 = new("pa_guardarTicketDetalle", con);
                        cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd2.Parameters.AddWithValue("@id_ticketMaestro", Convert.ToInt32(tabladeDatos.Rows[0]["ID_Ticket"]));

                        string codDESCRITPCIONTIK = tabladeDatos.Rows[0]["COD_TK"].ToString();
                        string conCATENACIONNUMERO = tabladeDatos.Rows[0]["ultimaNumeracion"].ToString();
                        string concat = codDESCRITPCIONTIK + "-" + conCATENACIONNUMERO;

                        cmd2.Parameters.AddWithValue("@descripcionCODTK",concat);
                        cmd2.Parameters.AddWithValue("@fechaexpiracion",DateTime.Now);
                        cmd2.Parameters.AddWithValue("@estado","En Espera");
                        cmd2.Parameters.AddWithValue("@ID_nuevoVI", ID_visitante);

                        // pasando valores al objeto 

                        objetoIMPRIMIR.AreaVisitar = tabladeDatos.Rows[0]["AREAVISITAR"].ToString();
                        objetoIMPRIMIR.Turno = concat;
                       

                        objetoIMPRIMIR.Fecha = devolverfecha();
                        await cmd2.ExecuteNonQueryAsync();

                        int codTKmaestro = Convert.ToInt32(tabladeDatos.Rows[0]["ID_Ticket"]);
                        
                        actualizarUltimoTIcket(codTKmaestro);

                        return objetoIMPRIMIR;
                    }
                }
                
            }
        }

        // metodo para actualizar el ultimaNUmeracion
        public async void actualizarUltimoTIcket(int ID_TK_MAESTRO)
        {
            using (SqlConnection con = new SqlConnection(_con2))
            {
                await con.OpenAsync();
                SqlCommand cmd = new("sumarenUNoUltimaNumeracion", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id_ticket", ID_TK_MAESTRO);

                await cmd.ExecuteNonQueryAsync();
            }
        }

        // metodo que compruebe que el cod de de servicio existe
        public async Task<bool> comprobarSiexisteelCodServ(string codServ)
        {
            using (SqlConnection con = new SqlConnection(_con2))
            {
                await con.OpenAsync();
                SqlCommand cmd = new("comprobar_codServ", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@cod_servicio", codServ);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    if (reader.HasRows)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                  
                }
               
            }
        }
        // metodo para calcular la fecha
        public string devolverfecha()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-us", false);
            int ano = DateTime.Now.Year;
            int mes = DateTime.Now.Month;
            int dia = DateTime.UtcNow.Day;
            int hora = 23;
            int minuto = 59;
            int segundo = 59;
            DateTime fechadevo = new DateTime(ano, mes, dia, hora, minuto, segundo);
            string fechadevolucion = ""+dia+"/"+mes+"/"+ano+"T"+hora+":"+minuto+":"+segundo;
            //return fechadevo;
            return fechadevolucion;
        }

        public async Task<string> llamado_ticket(string cod_TK, string useremail, string ComputadoraTerminal)
        {
            DateTime fecha_llamado = DateTime.Now;
            string Retorno = "";
            // verificnado que exista el tk
            if (await si_existeTK(cod_TK) == true)
            {
                if (await si_existeTerminal(ComputadoraTerminal)==true)
                {
                    using (SqlConnection con = new SqlConnection(_con2))
                    {
                        await con.OpenAsync();
                        SqlCommand cmd = new("LLamandoTk_Atendiendo", con);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@descripcion_codTK", cod_TK);
                        cmd.Parameters.AddWithValue("@fecha_actual", fecha_llamado);
                        cmd.Parameters.AddWithValue("@usuario", useremail);
                        cmd.Parameters.AddWithValue("@NombreTerminal", ComputadoraTerminal);

                        using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            string valorTKcomprobar = "";

                            if (reader.HasRows)
                            {
                                while (await reader.ReadAsync())
                                {
                                    valorTKcomprobar = reader.GetString(0);// necesita un ciclo para leer el valor
                                                                                         // valorTKcomprobar = reader.GetString(0);
                                }

                                if (valorTKcomprobar.Equals(cod_TK))
                                {
                                    Retorno = cod_TK;
                                    return Retorno;
                                }
                                else
                                {
                                    if (valorTKcomprobar == "La ticket esta siendo atendida")
                                    {
                                        return valorTKcomprobar;
                                    }
                                    else 
                                    {
                                        return Retorno= "La ticket esta vencida";
                                    }

                                }

                            }
                            else
                            {
                                return Retorno = "Error con la ticket";
                            }

                        }
                    }
                }
                else
                {
                   return Retorno = "La Terminal que desea atender la ticket esta inactiva o no existe";
                }
            }
            else
            {
                 return Retorno = "La Ticket no existe Ó ESTA VENCIDA";
            }
            
        }
        // metodo para comprobar que exista el ticke
        public async Task<bool> si_existeTK(string cod_tk)
        {
            using (SqlConnection con = new SqlConnection(_con2))
            {
                bool bandera = false;
                DateTime fechaActual = DateTime.Now;
                await con.OpenAsync();
                SqlCommand cmd = new("VerificarSi_existeTK", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DescripcionCod_tk", cod_tk);
                cmd.Parameters.AddWithValue("@fecha_actual", fechaActual);
                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    if (reader.HasRows)
                    {
                        return bandera = true;
                    }
                    else
                    {
                        return bandera;
                    }
                }
            }
        }
        // metodo para verificar que la terminal esta activa y exista
        public async Task<bool> si_existeTerminal(string nombreTerminal)
        {
            using (SqlConnection con = new SqlConnection(_con2))
            {
                bool bandera = false;
                await con.OpenAsync();
                SqlCommand cmd = new("VerificarSi_existeESCRITORIO", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@nombreterminal", nombreTerminal);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    if (reader.HasRows)
                    {
                        return bandera = true;
                    }
                    else
                    {
                        return bandera;
                    }
                }
            }
        }

        public async Task<bool> marcar_tkAtendido(string cod_TK)
        {
            
            bool bandera = await ejecutar_SQL_LLAMADOSTK(cod_TK, "LLamandoTk_Atendido");
            return bandera;
        }

        public async Task<bool> marcar_tkSE_RETIRO(string cod_TK)
        {
            bool bandera = await ejecutar_SQL_LLAMADOSTK(cod_TK, "LLamandoTk_RETIRO");
            return bandera;
        }

        public async Task<bool> marcar_tkVOLVER_ALLAMAR(string cod_TK)
        {
            bool bandera = await ejecutar_SQL_LLAMADOSTK(cod_TK, "LLamandoTk_VolverLlamar");
            return bandera;
        }

        // metodos del llamado de las tickes ESTE METODO ES GENERICO TOMA LA FECHA Y EL COD DE TK Y EJECUTA LA CONSULTA

        public async Task<bool> ejecutar_SQL_LLAMADOSTK(string cod_TK, string nombre_sp)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(_con2))
                {
                    await con.OpenAsync();
                    SqlCommand cmd = new(nombre_sp, con);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@descripcion_codTK", cod_TK);


                    await cmd.ExecuteNonQueryAsync();
                    return true;
                }
            }
            catch (Exception ex)
            {
                string mensaje = ex.Message.ToString();
                return false;
            }
        }

    }
}
