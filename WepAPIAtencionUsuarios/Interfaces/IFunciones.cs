﻿using WepAPIAtencionUsuarios.Entidades;
using Microsoft.Data.SqlClient;

namespace WepAPIAtencionUsuarios.Interfaces
{
    public interface IFunciones
    {

         Task<List<TiposIdentificacion>> cargarTipoIdentificacion();
         Task<List<PersonaPadron>> Buscarxceduda(string cedula);
       
    }
}
