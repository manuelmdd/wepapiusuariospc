﻿using WepAPIAtencionUsuarios.Entidades;

namespace WepAPIAtencionUsuarios.Interfaces
{
    public interface IVisitante
    {
        Task<int> InsertNuevovistante(NuevoVisitante personavisitant);

        Task<bool> verificarAcompañante(int cod_visitante, NuevoVisitante personavisitant);
        Task<visitantedcab> Mostrarvistante(int id_visitante);
    }
}
