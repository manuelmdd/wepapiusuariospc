﻿using Microsoft.AspNetCore.Mvc;
using WepAPIAtencionUsuarios.Entidades;

namespace WepAPIAtencionUsuarios.Interfaces
{
    public interface IFuncionesTickets
    {
        Task<int> nuevaTicketMaestro(Ticket ticket, string emailUser);

        Task<TicketDevuelto> mostrarTicketMaestro(int id_ticket);

        Task<ImpresionTicket> impresionTicketDetalle(string areaVisitar, int ID_visitante);

        Task<string> llamado_ticket(string cod_TK, string user, string nombreTerminal);

        Task<bool> marcar_tkAtendido(string cod_TK);

        Task<bool> marcar_tkSE_RETIRO(string cod_TK);

        Task<bool> marcar_tkVOLVER_ALLAMAR(string cod_TK);


    }
}
