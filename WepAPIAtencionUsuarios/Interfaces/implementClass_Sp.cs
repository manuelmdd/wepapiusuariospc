﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Identity.Client;
using System.Collections.Generic;
using System.Data;
using WepAPIAtencionUsuarios.Entidades;

namespace WepAPIAtencionUsuarios.Interfaces
{
    public class implementClass_Sp : IProcedimientosSp
    {
        
        private readonly string? _con2;

        public implementClass_Sp(IConfiguration configuracion)
        {
            this._con2 = configuracion.GetConnectionString("ConSCA1");
           
        }
     
        public async Task<List<areasVisitar>> Sp_Areas_Mostrar()
        {
            List<areasVisitar> listAreas = new List<areasVisitar>();

            int punteroLista = 0;

            int tamRegistro = 0;



            using (SqlConnection con = new SqlConnection(_con2))
                {
                    await con.OpenAsync();
                    
                    
                    SqlCommand cmd = new("pa_listarAreasIngresar2", con);
                    cmd.CommandTimeout = 25;
                    
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        DataTable tablaDatos = new DataTable();
                        tablaDatos.Load(reader);

                        string nombreColumna = "ID_SERVICIO";
                        int i;
                    for ( i = 0; i < tablaDatos.Rows.Count - 1;)
                    {
                        
                        object valoractual = tablaDatos.Rows[i][nombreColumna];

                        // valor siguiente
                        object valorSiguiente = tablaDatos.Rows[i + 1 ][nombreColumna];
                        
                        if (valoractual.Equals(valorSiguiente))
                        {

                            tamRegistro = tamaMotivos(tablaDatos,nombreColumna,valoractual,i-1);
                            llenarXvariosMotivos(i,listAreas,tablaDatos,tamRegistro);
                           
                        }
                        else
                        {
                            llenarUnRegistro(i,listAreas,tablaDatos);
                            punteroLista = punteroLista + 1;
                        }

                        //reinciiando el puntero i
                        if (i != 0)
                        {
                            punteroLista = punteroLista + tamRegistro;
                            i = punteroLista;
                            tamRegistro = 0;
                        }
                        else
                        {
                            i++;
                        }
                       
                    }
                    // aca comprobamos cuando llegamos al ultimo registro
                    if (i == tablaDatos.Rows.Count - 1)
                    {
                        llenarUnRegistro(i,listAreas,tablaDatos);
                    }
                        
                        await reader.CloseAsync();
                       
                    }
                await con.CloseAsync();
            }
            return listAreas;
        }

        public async Task<List<areasVisitar>> Sp_detalleAreas(string nombrearea, string codRegistroPub)
        {
            List<areasVisitar> listadetalServ = new List<areasVisitar>();
            using (SqlConnection con = new SqlConnection(_con2))
            {
                await con.OpenAsync();
                SqlCommand cmd = new("pa_listarAreasIngresar_serv_espec", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@descripciondelservicio", nombrearea);
                cmd.Parameters.AddWithValue("@nombreRegistroPublico", codRegistroPub);

                using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                {
                    DataTable tablaDatos = new DataTable();
                    tablaDatos.Load(reader);
                    int tamanoRegistro = tablaDatos.Rows.Count;
                    
                    llenarXvariosMotivos(0,listadetalServ,tablaDatos,tamanoRegistro);
                }
            }
            return listadetalServ;
        }
        // metodo para llenar un registro a la lista sin motivos
        public void llenarUnRegistro(int indice, List<areasVisitar> lista, DataTable tablaDatos)
        {
            detalleMotivos objetoMotivos = new detalleMotivos();

            areasVisitar objAreassinMOtivos = new areasVisitar();

            objAreassinMOtivos.ID_servicio = Convert.ToInt32(tablaDatos.Rows[indice]["ID_SERVICIO"]);
            objAreassinMOtivos.Cod_Servicio = Convert.ToString(tablaDatos.Rows[indice]["COD_SERV"]);
            objAreassinMOtivos.Des_Servicio = Convert.ToString(tablaDatos.Rows[indice]["AREAVISITAR"]);
            objAreassinMOtivos.detalleMotivos.Add(llenarDetalle(tablaDatos.Rows[indice], objetoMotivos));
            lista.Add(objAreassinMOtivos);
        }

        // metodo para llenar el registro con mas de 1 motivo
        public void llenarXvariosMotivos(int indice, List<areasVisitar> lista, DataTable tablaDatos, int tamRegistro)
        {
            areasVisitar objetoAreasLista = new areasVisitar();
            objetoAreasLista.ID_servicio = Convert.ToInt32(tablaDatos.Rows[indice]["ID_SERVICIO"]);
            objetoAreasLista.Cod_Servicio = Convert.ToString(tablaDatos.Rows[indice]["COD_SERV"]);
            objetoAreasLista.Des_Servicio = Convert.ToString(tablaDatos.Rows[indice]["AREAVISITAR"]);

            int contadoractual = 1;
            int ee = indice;

            for (int ii = ee; contadoractual <= tamRegistro; ii++)
            {
                detalleMotivos objetoMotivos = new detalleMotivos();

                objetoAreasLista.detalleMotivos.Add(llenarDetalle(tablaDatos.Rows[ii], objetoMotivos));
                contadoractual++;
            }

            lista.Add(objetoAreasLista);
        }

        // metodos de busquedad
        public detalleMotivos llenarDetalle(DataRow reader, detalleMotivos obje)
        {
            obje.codigotabla = Convert.ToInt32(reader["CODIGOTABLA"]);
            obje.prioridadMotivo = Convert.ToString(reader["PRIORIDAD_MOTIVO"]);
            return obje;
        }

        public int tamaMotivos(DataTable tabla, string columna, object valorcomparar, int indice)
        {
            int tamano = 0;

            for (int i = indice; i < tabla.Rows.Count - 1; i++)
            {
                object valorSiguiente = tabla.Rows[i + 1][columna];

                if (valorcomparar.Equals(valorSiguiente))
                {
                    tamano++;
                }
                else
                {
                    break;
                }
            }
            return tamano;
        }
    }
}
