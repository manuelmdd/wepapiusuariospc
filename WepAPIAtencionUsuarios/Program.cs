
using WepAPIAtencionUsuarios;

var builder = WebApplication.CreateBuilder(args);

// instanciadno clase startup

var startup = new Startup(builder.Configuration);

startup.ConfigureServices(builder.Services);

var app = builder.Build();

startup.Configure(app,app.Environment);

app.Run();
