﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace WepAPIAtencionUsuarios.Middleware
{
    public class FiltrodeExcepcion : ExceptionFilterAttribute
    {
        private readonly ILogger<FiltrodeExcepcion> logger;
        public FiltrodeExcepcion(ILogger<FiltrodeExcepcion> logger)
        {
            this.logger = logger;
        }

        public override void OnException(ExceptionContext context)
        {
            logger.LogError(context.Exception, context.Exception.Message);
            logger.LogInformation(context.Exception.Message);
            base.OnException(context);
        }
    }
}
